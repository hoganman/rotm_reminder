#!/usr/bin/env bash
# azure resource setup -- run once

storage_account=rotmreminder$(date +%s)
sid="twilio sid"
auth_token="twilio auth token"
tpn="twilio phone number"
sms_number="number to be notified"
cron_schedule="0 */30 * * * *"

echo "Storage account=$storage_account"
az login
az group create --name RotmReminder-rg --location australiaeast
az storage account create --name $storage_account --location australiaeast --resource-group RotmReminder-rg --sku Standard_LRS
az functionapp create --resource-group RotmReminder-rg --os-type Linux --consumption-plan-location australiaeast --runtime python --runtime-version 3.7 --functions-version 2 --name RotmReminder-fa --storage-account $storage_account
func azure functionapp publish RotmReminder-fa

# list current config
az functionapp config appsettings list -n RotmReminder-fa -g RotmReminder-rg

# get the storage account connection string created for function
storage_connect_string=$(az functionapp config appsettings list -n RotmReminder-fa -g RotmReminder-rg --query "([?name=='AzureWebJobsStorage'].value)[0]")

# set config items
az functionapp config appsettings set -n RotmReminder-fa -g RotmReminder-rg --settings="AZURE_STORAGE_CONNECTION_STRING=$storage_connect_string"
az functionapp config appsettings set -n RotmReminder-fa -g RotmReminder-rg --settings="TWILIO_SID=$sid"
az functionapp config appsettings set -n RotmReminder-fa -g RotmReminder-rg --settings="TWILIO_AUTH_TOKEN=$auth_token"
az functionapp config appsettings set -n RotmReminder-fa -g RotmReminder-rg --settings="TWILIO_PHONE_NUMBER=$tpn"
az functionapp config appsettings set -n RotmReminder-fa -g RotmReminder-rg --settings="SMS_NOTIFICATION_PHONE_NUMBER=$sms_number"
az functionapp config appsettings set -n RotmReminder-fa -g RotmReminder-rg --settings="CRON_SCHEDULE=$cron_schedule"