import unittest
import unittest.mock as mock
import responses
from azure.core.exceptions import ResourceExistsError, ResourceNotFoundError
import src.TimerCheckForNewRotm

html_template = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>{body}</body>
</html>
"""

html_image_1 = '<img alt="ROTM" src="image1"/>'
html_image_2 = '<img alt="ROTM" src="image2"/>'


@mock.patch('azure.storage.blob.BlobServiceClient.from_connection_string')
@mock.patch('src.sms.send')
class TimerCheckForNewRotmTestCase(unittest.TestCase):

    mock_timer_request = mock.MagicMock()

    @responses.activate
    def test_run_trigger_first_time(self, mock_sms_send, mock_bsc):
        """
        This simulates what happens when the function runs for the first time
        At this point the blob container or blob does not exist
        This should trigger an SMS to indicate that the service is active
        """
        # coopers site returns image 1
        responses.add(
            responses.GET, 'https://www.diybeer.com/au/recipes',
            body=html_template.format(body=html_image_1))

        mock_bsc_instance = mock_bsc.return_value

        # retrieval of blob fails -- resource not found
        mock_bsc_instance.get_container_client.return_value\
            .get_blob_client.return_value\
            .download_blob.side_effect = ResourceNotFoundError(message="Blob not found")

        src.TimerCheckForNewRotm.main(self.mock_timer_request)

        # check that sms sent -- notifications enabled
        self.assertEqual(mock_sms_send.mock_calls.pop(), mock.call('Coopers ROTM recipe notification now enabled'))

        # check that upload of page occurred
        print(mock_bsc_instance.mock_calls.pop())
        mock_bsc_instance.get_container_client().get_blob_client().upload_blob.assert_called_with(
            mock.ANY, overwrite=True)

    @responses.activate
    def test_run_trigger_past_due(self, mock_sms_send, mock_bsc):
        """
        Timers can be triggered out of usual time boundaries.
        This does not impact the way this function operates
        """
        responses.add(responses.GET, 'https://www.diybeer.com/au/recipes',
                      body=html_template.format(body=html_image_1))
        self.mock_timer_request.past_due = False

        mock_bsc_instance = mock_bsc.return_value
        mock_bsc_instance.get_container_client.return_value\
            .get_blob_client.return_value\
            .download_blob.return_value\
            .readall.return_value\
            .decode.return_value = html_template.format(body=html_image_1)
        src.TimerCheckForNewRotm.main(self.mock_timer_request)

    @responses.activate
    def test_run_trigger_coopers_error(self, mock_sms_send, mock_bsc):
        """
        Simulate when call to get new page content at coopers fails
        """
        # don't define response will raise connection error
        mock_bsc_instance = mock_bsc.return_value
        mock_bsc_instance.get_container_client.return_value\
            .get_blob_client.return_value\
            .download_blob.return_value\
            .readall.return_value\
            .decode.return_value = html_template.format(body=html_image_1)
        src.TimerCheckForNewRotm.main(self.mock_timer_request)

        # try a 400 error
        responses.add(responses.GET, 'https://www.diybeer.com/au/recipes', status=400)
        src.TimerCheckForNewRotm.main(self.mock_timer_request)

    @responses.activate
    def test_run_trigger_no_change(self, mock_sms_send, mock_bsc):
        """
        This simulates when the old and new rotm image are the same
        No SMS is sent
        """
        # response from coopers -- returns image 1
        responses.add(
            responses.GET, 'https://www.diybeer.com/au/recipes',
            body=html_template.format(body=html_image_1))

        mock_bsc_instance = mock_bsc.return_value

        # exception when creating container - already exists
        mock_bsc_instance.create_container = mock.MagicMock(side_effect=ResourceExistsError)

        # get old page -- returns image 1
        mock_bsc_instance.get_container_client.return_value\
            .get_blob_client.return_value\
            .download_blob.return_value\
            .readall.return_value\
            .decode.return_value = html_template.format(body=html_image_1)

        src.TimerCheckForNewRotm.main(self.mock_timer_request)

        # check that SMS not sent
        self.assertEqual(0, len(mock_sms_send.mock_calls))
        # check page not uploaded
        mock_bsc_instance.get_container_client().get_blob_client().upload_blob.assert_not_called()

    @responses.activate
    def test_run_trigger_change_detected(self, mock_sms_send, mock_bsc):
        """
        This simulates when the image changes from what is in blob storage
        SMS is sent stating that new recipe is available
        """
        # response from coopers -- returns image 2
        responses.add(responses.GET, 'https://www.diybeer.com/au/recipes',
                      body=html_template.format(body=html_image_2))

        mock_bsc_instance = mock_bsc.return_value

        # exception when creating container - already exists
        mock_bsc_instance.create_container = mock.MagicMock(side_effect=ResourceExistsError)

        # old page -- returns image 1
        mock_bsc_instance.get_container_client.return_value\
            .get_blob_client.return_value\
            .download_blob.return_value\
            .readall.return_value\
            .decode.return_value = html_template.format(body=html_image_1)

        src.TimerCheckForNewRotm.main(self.mock_timer_request)

        # check that sms sent -- recipe released
        self.assertEqual(
            mock_sms_send.mock_calls.pop(), mock.call('Looks like a new Coopers ROTM recipe has been released'))
        # check that new page uploaded
        mock_bsc_instance.get_container_client().get_blob_client().upload_blob.assert_called_with(
            mock.ANY, overwrite=True)


if __name__ == '__main__':
    unittest.main()
