# rotm_reminder

Microsoft Azure function that sends an SMS to your mobile when a new Coopers DIY Beer recipe of the month (rotm) is released.

## Architecture

                    ------------
                    | blob     |
                    | storage  |
                    ------------
                         ^
                         |
    --------        ------------  https
    | cron |-=-=-=->| python   |--------> Coopers
    --------  trig  | function |--------> Twilio
                    ------------  https
                       

A single Azure function is triggered by a TimedTrigger according to a `cron` schedule.

Currently the schedule is set to run every 30 minutes.

The function manages a copy of the last Coopers page in blob storage.

When the function runs, it retrieves the last version from the blob and 
gets the current version from the Coopers site.

It parses both old an new versions to retrieve the ROTM image tags in the page.

If the tags are different, then it uses Twilio to send SMS notification
and stores the new version page to the blob storage replacing the previous version.
 

## Local development

### install

The app has been built using Azure functions and requires some components to be installed for local development.

* Python 3, and your favorite env manager 

* Install required python modules


    pip install -r src/requirements.txt

* Azure Function core tools -- Allows you to run Azure functions locally. See https://github.com/Azure/azure-functions-core-tools

    
    func --version

* Azurite -- Azure storage and queue simulator. See https://docs.microsoft.com/en-us/azure/storage/common/storage-use-azurite?toc=/azure/storage/blobs/toc.json

    Docker option is assumed and used in this documentation, but any of the options provided can be used and substituted.


    docker run -p 10000:10000 -p 10001:10001 mcr.microsoft.com/azure-storage/azurite

* Twilio account -- The app uses Twilio to send the SMS. You will need an account to access this service.

* Microsoft Azure Storage Explorer (optional) -- If you want to see what is being stored in Azurite blob storage

### test

    pip install -r requirements_dev.txt
    pytest


### run

Create `local.settings.json` in the `src` directory.
These values are exposed to the python functions as environment variables when run locally with `func start`.

    {
      "IsEncrypted": false,
      "Values": {
        "FUNCTIONS_WORKER_RUNTIME": "python",
        "AzureWebJobsStorage": "UseDevelopmentStorage=true",
        "AZURE_STORAGE_CONNECTION_STRING": "UseDevelopmentStorage=true",
        "TWILIO_SID": "your-twilio-sid",
        "TWILIO_AUTH_TOKEN": "your-twilio-auth-token",
        "TWILIO_PHONE_NUMBER": "Your allocated Twilio number",
        "SMS_NOTIFICATION_PHONE_NUMBER": "number where notification sent",
        "CRON_SCHEDULE": "0/10 * * * * *"
      }
    }

Run the following commands to run the functions locally.

     cd src
     docker run -p 10000:10000 -p 10001:10001 mcr.microsoft.com/azure-storage/azurite
     func start

## Cloud deployment

Install Azure CLI -- Interact with Azure services. See https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest

Review and run script commands in `setup.sh`.
The script has Azure cli commands to perform the initial project setup.

### environment variables
The following parameters needs to be setup in the Azure function config.

This can be done via the Azure CLI using the following command:

    az functionapp config appsettings set -n RotmReminder-fa -g RotmReminder-rg --settings="<variable name>=<variable value>"


|  variable name                     |   description            | default value |
|------------------------------------|--------------------------|---------------|
| AzureWebJobsStorage                | connection string for Function trigger blob storage. See https://docs.microsoft.com/en-us/azure/storage/common/storage-configure-connection-string | Configured automatically as part of function setup|
| AZURE_STORAGE_CONNECTION_STRING    | connection string for Function blob storage. See https://docs.microsoft.com/en-us/azure/storage/common/storage-configure-connection-string | Should be set to same value as AzureWebJobsStorage |
| ROTM_NOTIFICATION_STORAGE_CONTAINER| name of the storage container used for function state | `rotm-reminder`
| TWILIO_SID                         | twilio sid               | |
| TWILIO_AUTH_TOKEN                  | twilio auth token        | |
| TWILIO_PHONE_NUMBER                | twilio phone number      | |
| SMS_NOTIFICATION_PHONE_NUMBER      | Number to receive notification | |
| CRON_SCHEDULE                      | cron schedule to trigger check | `0 */30 * * * *` |

### bitbucket pipeline setup
Setup a service principal to perform Azure function deployment from pipelines.

    az ad sp create-for-rbac --name BitbucketDeploymentPrincipal
    
Save the details returned from this command and provide as variables to 
the Bitbucket pipeline repository variables.

|  variable name         |   description                 | default value |
|------------------------|-------------------------------|---------------|
| SP_APP_ID              |  Service principal app id     | |
| SP_PASSWORD            |  Service principal password   | |
| SP_TENANT_ID           |  Service principal tennant id | |