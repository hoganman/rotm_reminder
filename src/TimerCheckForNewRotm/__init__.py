import datetime
import io
import logging

import azure.functions as func
import requests
from azure.core.exceptions import ResourceExistsError, ResourceNotFoundError
from azure.storage.blob import BlobServiceClient
from bs4 import BeautifulSoup

from .. import config
from .. import sms


def main(mytimer: func.TimerRequest) -> None:
    last_page_blob = 'last_page.html'

    utc_timestamp = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
    logging.info('rotm-notification timer trigger function ran at %s', utc_timestamp)

    if mytimer.past_due:
        logging.info('The timer is past due!')

    logging.debug(f'AZURE_STORAGE_CONNECTION_STRING={config.AZURE_STORAGE_CONNECTION_STRING}')

    # Create the BlobServiceClient object which will be used to create a container client
    blob_service_client = BlobServiceClient.from_connection_string(config.AZURE_STORAGE_CONNECTION_STRING)

    try:
        blob_service_client.create_container(config.ROTM_NOTIFICATION_STORAGE_CONTAINER)
        logging.debug('Container created')
    except ResourceExistsError:
        logging.debug('Container exists')

    container_client = blob_service_client.get_container_client(config.ROTM_NOTIFICATION_STORAGE_CONTAINER)

    # get last page data from blob
    blob_client = container_client.get_blob_client(blob=last_page_blob)
    try:
        last_page_blob_data = blob_client.download_blob().readall().decode() if blob_client else None
    except ResourceNotFoundError:
        last_page_blob_data = ''
    logging.debug(last_page_blob_data)
    old_soup = BeautifulSoup(last_page_blob_data, 'html.parser')
    old_rotm = old_soup.find("img", alt="ROTM")
    logging.debug(old_rotm)

    # get latest data from coopers
    try:
        new_page_response = requests.get('https://www.diybeer.com/au/recipes')
        new_page_response.raise_for_status()
    except requests.exceptions.RequestException:
        # if you get an error retrieving page content from Coopers, exit
        logging.error('Unable to reach coopers site, exiting without further processing')
        return

    new_soup = BeautifulSoup(new_page_response.text.encode(), 'html.parser')
    new_rotm = new_soup.find("img", alt="ROTM")
    logging.debug(new_rotm)
    change_detected = old_rotm and old_rotm != new_rotm
    logging.info(f'Change detected: {change_detected}')

    # Replace stored page contents when change or no old content.
    if change_detected or not old_rotm:
        logging.info('Saving new copy of web page')
        file_data = io.BytesIO(new_page_response.text.encode())
        file_data.seek(0)
        blob_client.upload_blob(file_data, overwrite=True)

    # send SMS notification when change
    if change_detected:
        logging.info('Sent SMS notification - change detected')
        sms.send('Looks like a new Coopers ROTM recipe has been released')

    # first run. send text to confirm is working
    if not old_rotm:
        logging.info('Sent SMS notification - first run')
        sms.send('Coopers ROTM recipe notification now enabled')
