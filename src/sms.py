import logging
from twilio.rest import Client
try:
    from . import config
except ImportError:
    import config


def send(message_text):
    # Initialize the Twilio client.
    client = Client(config.TWILIO_SID, config.TWILIO_AUTH_TOKEN)

    # Send the SMS message.
    message = client.messages.create(
        to=config.SMS_NOTIFICATION_PHONE_NUMBER,
        from_=config.TWILIO_PHONE_NUMBER,
        body=message_text)

    logging.info(message.sid)


if __name__ == '__main__':
    send('This is a test message')
